#!/bin/env node
const random = (a = 0, b = 1) => Math.random() * (b - a) + a;
const random_int = (a, b) => Math.round(random(a, b));

const $ = process.argv.slice(2);
console.log(random_int(50000, 80000));